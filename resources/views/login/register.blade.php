<!doctype html>
<html lang="en">
  <!--Head-->
  @include('login.include.head')
  <!--/Head-->
  <body>
    <div class="content" id="app">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="{{asset('/images/undraw_remotely_2j6y.svg')}}" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-6 contents">
            <div class="row justify-content-center">
              <div class="col-md-8">
                <div class="mb-4">
                  <h3>Register</h3>
                  <p class="mb-4">Silahkan Register Akun Anda</p>
                </div>
                <form action="#" method="post" action="{{route ('register')}}">
                    {{csrf_field()}}
                    <div class="form-group first">
                        <label for="name">Name</label>
                        <input type="text" class="form-control {{$errors->has('name')?'is-invalid':'' }}" id="name" name="name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{$errors->first('name')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group first">
                        <label for="email">Email</label>
                        <input type="email" class="form-control {{$errors->has('email')?'is-invalid':'' }}" id="email" name="email" value="{{old('email')}}">
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                {{$errors->first('email')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group last mb-4">
                        <label for="password">Password</label>
                        <input type="password" class="form-control {{$errors->has('password')?'is-invalid':'' }}" id="password" name="password" v-model="input_password">
                        @if($errors->has('password'))
                            <div class="invalid-feedback">
                                {{$errors->first('password')}}
                            </div>
                        @endif
                    </div>
                    <input type="submit" value="Register" class="btn btn-block btn-primary">
                    <span class="d-block text-left my-4 text-muted">&mdash; or login with &mdash;</span>
                    <div class="social-login">
                        <a href="#" class="facebook">
                        <span class="icon-facebook mr-3"></span> 
                        </a>
                        <a href="#" class="twitter">
                        <span class="icon-twitter mr-3"></span> 
                        </a>
                        <a href="#" class="google">
                        <span class="icon-google mr-3"></span> 
                        </a>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Head-->
    @include('login.include.script')
    <!--/Head-->
  </body>
</html>