<!DOCTYPE html>
<html lang="en">

    <!--Head-->
    @include('user.include.head')    
    <!--End Head-->

    <body>
        <!-- Header -->
        @include('user.include.header') 
        <!-- Close Header -->

        <!-- Content -->
        @yield('content')
        <!-- Close Content -->

        <!-- Start Footer -->
        @include('user.include.footer') 
        <!-- End Footer -->

        <!-- Start Script -->
        @include('user.include.script') 
        <!-- End Script -->
    </body>
</html>