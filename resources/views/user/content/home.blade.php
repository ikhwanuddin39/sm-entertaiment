@extends('user.user')

@section('content')
    <!-- Slideshow -->
    <div class="banner-wrapper bg-light">
        <div id="index_banner" class="banner-vertical-center-index container-fluid pt-5">
            <!-- Start slider -->
            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
                    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
                    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="py-5 row d-flex align-items-center">
                            <div class="banner-content col-lg-8 col-8 offset-2 m-lg-auto text-left py-5 pb-5">
                                <h3 class="banner-heading h3 text-secondary display-3 mb-0 pb-3 mx-0 px-0 light-300">
                                    THE FUTURE OF CULTURE TECHNOLOGY 
                                </h3>
                                <h1 class="banner-heading h1 text-secondary display-3 mb-0 pb-5 mx-0 px-0 light-300 typo-space-line">
                                    ASIA'S NO.1
                                    <br>ENTERTAINMENT GROUP
                                </h1>
                                <p class="banner-body text-muted py-3 mx-0 px-0">
                                    SM Entertainment / SM JAPAN / SM USA / SM TRUE / DREAM MAKER / DREAM MAKER China
                                    SM C&C / SM Life Design Group / SM Brand Marketing
                                    DearU / KEYEAST / Stream Media Corp. / MYSTIC STORY / GalaxiaSM / ESteem
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">

                        <div class="py-5 row d-flex align-items-center">
                            <div class="banner-content col-lg-8 col-8 offset-2 m-lg-auto text-left py-5 pb-5">
                                <h3 class="banner-heading h3 text-secondary display-3 mb-0 pb-3 mx-0 px-0 light-300">
                                    DIGITAL CONVERGENCE 
                                </h3>
                                <h1 class="banner-heading h1 text-secondary display-3 mb-0 pb-5 mx-0 px-0 light-300 typo-space-line">
                                    NEW PARADIGM
                                    <br>NEW PLATFORM
                                </h1>
                                <p class="banner-body text-muted py-3 mx-0 px-0">
                                    Korean entertainment agencies through Asia-wide distribution channels and global partners
                                </p>
                            </div>
                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="py-5 row d-flex align-items-center">
                            <div class="banner-content col-lg-8 col-8 offset-2 m-lg-auto text-left py-5 pb-5">
                                <h3 class="banner-heading h3 text-secondary display-3 mb-0 pb-3 mx-0 px-0 light-300">
                                    EVOLUTIONARY TRENDS
                                </h3>
                                <h1 class="banner-heading h1 text-secondary display-3 mb-0 pb-5 mx-0 px-0 light-300 typo-space-line">
                                    THAT LEAD TO
                                    <br>LIFE STYLE
                                </h1>
                                <p class="banner-body text-muted py-3 mx-0 px-0">
                                    Leading cultures in different areas including Food & Beverage, Amusement, Merchandise Shop and Travel
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <a class="carousel-control-prev text-decoration-none" href="#carouselExampleIndicators" role="button" data-bs-slide="prev">
                    <i class='bx bx-chevron-left'></i>
                    <span class="visually-hidden">Previous</span>
                </a>
                <a class="carousel-control-next text-decoration-none" href="#carouselExampleIndicators" role="button" data-bs-slide="next">
                    <i class='bx bx-chevron-right'></i>
                    <span class="visually-hidden">Next</span>
                </a>
            </div>
            <!-- End slider -->
        </div>
    </div>
    <!-- End Slideshow -->

    <!-- About Us -->
    <section class="bg-light w-100">
        <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-6 text-start">
                    <h1 class="h2 py-5 text-primary typo-space-line">About Us</h1>
                    <p class="light-300">
                        SM Entertainment, founded in 1995 by Head Producer Lee Soo Man, is the first company in the industry to introduce systematic casting, training, producing, and management systems, and it has been discovering unique content by pinpointing demands for music and cultural trends. SM Entertainment entered the global marketplace using globalization and localization strategies through culture technology and has become a leading entertainment company in Asia.
                        <br><br>
                        In 1997, SM Entertainment became the first company in the Korean entertainment industry to enter foreign markets and made remarkable achievements as the leader of Hallyu, or the Korean Wave.
                        <br><br>
                        SM Entertainment has successfully set foot in North America, South America, and Europe while maintaining its base in Asia, and has enhanced the national brand of Korea and promoted the growth of the culture industry. SM Entertainment is promoting the unique culture of Korea through avenues such as K-POP, the Korean alphabet, and Korean cuisine, through 'Made by SM' content all over the world, and is elevating the prestige of Korea by promoting the consumption of Korean brand products. In particular, SM Entertainment has focused on the value of culture that can lead the national economy and has contributed to its growth under the catchphrase, "Culture First, Economy Next." SM Entertainment will continue to lead the entertainment industry until Korea becomes a ‘Cultural Powerhouse' as well as an ‘Economic Powerhouse' based on the idea that our economy will reach its heights only when our culture wins the heart of the entire world.
                    </p>
                </div>
                <div class="col-lg-6">
                    <img src="./assets/img/Logo3-4.png">
                </div>
            </div>
        </div>
    </section>
    <!-- About Us -->
    
    <!-- Start Visi Misi -->
    <section class="banner-bg bg-primary py-5">
        <div class="container my-4">
            <div class="row text-center">

                <div class="objective col-lg-4">
                    <div class="objective-icon card m-auto py-4 mb-2 mb-sm-4 bg-secondary shadow-lg">
                        <i class="display-4 bx bxs-bulb text-light"></i>
                    </div>
                    <h2 class="objective-heading h3 mb-2 mb-sm-4 light-3000">Our Vision</h2>
                    <p class="light-3000">
                        Management Philosophy
                        The Future of Culture Technology!
                        As a company that produces the best in cultural content and entertainers by using state-of-the-art culture technologies, SM Entertainment aims to touch the people’s hearts and promote Korean culture in every corner of the world with the goal of contributing to national economic growth and a more affluent life for everyone.
                    </p>
                </div>

                <div class="objective col-lg-4 mt-sm-0 mt-4">
                    <div class="objective-icon card m-auto py-4 mb-2 mb-sm-4 bg-secondary shadow-lg">
                        <i class='display-4 bx bx-revision text-light'></i>
                    </div>
                    <h2 class="objective-heading h3 mb-2 mb-sm-4 light-3000">Our Mission</h2>
                    <p class="light-3000">
                        Management Vision and Implementation Method
                        Asia’s No.1 Entertainment Group
                        To grow into a total entertainment group representing Asia, SM Entertainment is pushing forward aggressively with the following strategies.
                    </p>
                </div>

                <div class="objective col-lg-4 mt-sm-0 mt-4">
                    <div class="objective-icon card m-auto py-4 mb-2 mb-sm-4 bg-secondary shadow-lg">
                        <i class="display-4 bx bxs-select-multiple text-light"></i>
                    </div>
                    <h2 class="objective-heading h3 mb-2 mb-sm-4 light-3000">Our Goal</h2>
                    <p class="light-3000">
                        Becoming the Best Digital Content Provider to Lead the Internet and Mobile Markets
                        The Korean Wave has spread quickly throughout the world through new media platforms, including YouTube, Facebook, and Twitter. SM Entertainment will share the ‘Made By SM’ content with the entire world and build a virtual community for the global generation.
                    </p>
                </div>

            </div>
        </div>
    </section>
    <!-- End Visi Misi -->

    <!-- Start Team Member -->
    <section class="container py-5">
        <h2 class="h2 text-black text-center py-5">Our Team</h2>
        <div class="pt-5 pb-3 d-lg-flex align-items-center gx-5">
            <div class="col-lg-3">
                <h2 class="h2 py-5 typo-space-line">Inside Director</h2>
                <p class="text-muted light-300">
                    Inside directors are members of the board and executives at the SM ENTERTAINMENT
                </p>
            </div>

            <div class="col-lg-9 row">
                <div class="team-member col-md-4">
                    <img class="team-member-img img-fluid rounded-circle p-4" src="./assets/img/team-01.jpg" alt="Card image">
                    <ul class="team-member-caption list-unstyled text-center pt-4 text-muted light-300">
                        <li>Lee, Sung Su</li>
                        <li>CEO</li>
                    </ul>
                </div>
                <div class="team-member col-md-4">
                    <img class="team-member-img img-fluid rounded-circle p-4" src="./assets/img/team-02.jpg" alt="Card image">
                    <ul class="team-member-caption list-unstyled text-center pt-4 text-muted light-300">
                        <li>Tak, Young Jun</li>
                        <li>CMO</li>
                    </ul>
                </div>
                <div class="team-member col-md-4">
                    <img class="team-member-img img-fluid rounded-circle p-4" src="./assets/img/team-03.jpg" alt="Card image">
                    <ul class="team-member-caption list-unstyled text-center pt-4 text-muted light-300">
                        <li>Park Jun Young</li>
                        <li>Inside Director</li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="pt-5 pb-3 d-lg-flex align-items-center gx-5">
            <div class="col-lg-9 row">
                <div class="team-member col-md-4">
                    <img class="team-member-img img-fluid rounded-circle p-4" src="./assets/img/team-04.jpg" alt="Card image">
                    <ul class="team-member-caption list-unstyled text-center pt-4 text-muted light-300">
                        <li>Chae Hee Man</li>
                        <li>Outside Director</li>
                    </ul>
                </div>
                <div class="team-member col-md-4">
                    <img class="team-member-img img-fluid rounded-circle p-4" src="./assets/img/team-02.jpg" alt="Card image">
                    <ul class="team-member-caption list-unstyled text-center pt-4 text-muted light-300">
                        <li>Ji Chang Hoon</li>
                        <li>Outside Director</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <h2 class="h2 py-5 typo-space-line">Outside Director</h2>
                <p class="text-muted light-300">
                    In comparison, outside directors are not executives at the SM ENTERTAINMENT. They are independent individuals selected for their experience and expertise in the relevant industry or sector
                </p>
            </div>
        </div>
        <div class="pt-5 pb-3 d-lg-flex align-items-center gx-5">
            <div class="col-lg-3">
                <h2 class="h2 py-5 typo-space-line">Auditor</h2>
                <p class="text-muted light-300">
                    person authorized to review and verify the accuracy of financial records and ensure that companies comply with tax laws
                </p>
            </div>
            <div class="col-lg-9 row">
                <div class="team-member col-md-4">
                    <img class="team-member-img img-fluid rounded-circle p-4" src="./assets/img/team-03.jpg" alt="Card image">
                    <ul class="team-member-caption list-unstyled text-center pt-4 text-muted light-300">
                        <li>Lee Gang Bok</li>
                        <li>Auditor</li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
    <!-- End Team Member -->

    <!-- Start Group Companies -->
    <section class="bg-secondary py-3">
        <div class="container py-5">
            <h2 class="h2 text-white text-center py-5">Group Companies</h2>
            <div class="row text-center">
                @foreach($companies as $key=>$companiess)
                    <div class="col-md-3 mb-3">
                        <div class="card partner-wap py-5">
                            <a href="#">
                                <img class="team-member-img img-fluid p-4" src="{{ url('/uploads/'.$companiess->gambar) }}" alt="Card image">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--End Group Companies-->

    <!-- Start Business Unit -->
    <section class="bg-light py-5 mb-5">
        <div class="container">
            <div class="recent-work-header row text-center pb-5">
                <h2 class="col-md-6 m-auto h2 semi-bold-600 py-5">Our Business Unit</h2>
            </div>      
            <!--Entertainment-->
            <div class="col-md-4 mb-3">
                <h2 class="h2 py-5 typo-space-line">Entertainment</h2>
            </div>
            <div class="row gy-5 g-lg-5 mb-4">
                <!-- Start Recent Work -->
                @foreach($business1 as $key=>$businesss)
                    <div class="col-md-4 mb-3">
                        <a href="#" class="recent-work card border-0 shadow-lg overflow-hidden">
                            <img class="recent-work-img card-img" src="{{ url('/uploads/'.$businesss->gambar) }}" alt="Card image">
                            <div class="recent-work-vertical card-img-overlay d-flex align-items-end">
                                <div class="recent-work-content text-start mb-3 ml-3 text-dark">
                                    <h3 class="card-title light-300">{{$businesss->nama_business}}</h3>
                                    <p class="card-text">{{$businesss->jenis_business}}</p>
                                </div>
                            </div>
                        </a> 
                    </div>
                @endforeach
                <!-- End Recent Work -->
            </div>
            <!--Commerce & Amusement-->
            <div class="col-md-4 mb-3">
                <h2 class="h2 py-5 typo-space-line">Commerce & Amusement</h2>
            </div>
            <div class="row gy-5 g-lg-5 mb-4">
                <!-- Start Recent Work -->
                @foreach($business2 as $key=>$businesss)
                    <div class="col-md-4 mb-3">
                        <a href="#" class="recent-work card border-0 shadow-lg overflow-hidden">
                            <img class="recent-work-img card-img" src="{{ url('/uploads/'.$businesss->gambar) }}" alt="Card image">
                            <div class="recent-work-vertical card-img-overlay d-flex align-items-end">
                                <div class="recent-work-content text-start mb-3 ml-3 text-dark">
                                    <h3 class="card-title light-300">{{$businesss->nama_business}}</h3>
                                    <p class="card-text">{{$businesss->jenis_business}}</p>
                                </div>
                            </div>
                        </a> 
                    </div>
                @endforeach
                <!-- End Recent Work -->
            </div>
            <!--Mobile Entertainment Platform-->
            <div class="col-md-4 mb-3">
                <h2 class="h2 py-5 typo-space-line">Mobile Entertainment Platform</h2>
            </div>
            <div class="row gy-5 g-lg-5 mb-4">
                <!-- Start Recent Work -->
                @foreach($business3 as $key=>$businesss)
                    <div class="col-md-4 mb-3">
                        <a href="#" class="recent-work card border-0 shadow-lg overflow-hidden">
                            <img class="recent-work-img card-img" src="{{ url('/uploads/'.$businesss->gambar) }}" alt="Card image">
                            <div class="recent-work-vertical card-img-overlay d-flex align-items-end">
                                <div class="recent-work-content text-start mb-3 ml-3 text-dark">
                                    <h3 class="card-title light-300">{{$businesss->nama_business}}</h3>
                                    <p class="card-text">{{$businesss->jenis_business}}</p>
                                </div>
                            </div>
                        </a> 
                    </div>
                @endforeach
                <!-- End Recent Work -->
            </div>
        </div>
    </section>
    <!-- End Business Unit -->

    <!-- Start SOCIAL NETWORK -->
    <section class="bg-white py-5">
        <div class="container my-4">

            <h1 class="creative-heading h2 pb-3">SOCIAL NETWORK</h1>

            <div class="creative-content row py-3">
                <div class="col-md-5">
                    <p class="text-muted col-lg-8 light-300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida Risus.
                    </p>
                </div>
                <div class="creative-progress col-md-7">

                    <div class="row mt-4 mt-sm-2">
                        <div class="col-6">
                            <h4 class="h5">Facebook</h4>
                        </div>
                        <div class="col-6 text-right">45 M Likes</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                    <div class="row mt-4 mt-sm-2">
                        <div class="col-6">
                            <h4 class="h5">Twitter</h4>
                        </div>
                        <div class="col-6 text-right">8.5 M Followers</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 8.5%" aria-valuenow="8.5" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>


                    <div class="row mt-4 mt-sm-2">
                        <div class="col-6">
                            <h4 class="h5">Youtube</h4>
                        </div>
                        <div class="col-6 text-right">43.8 Billion Views</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 43%" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    
                    <div class="row mt-4 mt-sm-2">
                        <div class="col-6">
                            <h4 class="h5">Weibo</h4>
                        </div>
                        <div class="col-6 text-right">15 M Followers</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                    <div class="row mt-4 mt-sm-2">
                        <div class="col-6">
                            <h4 class="h5">Baidu Community</h4>
                        </div>
                        <div class="col-6 text-right">8.43 M Followers</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 8.4%" aria-valuenow="8.4" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                    <div class="row mt-4 mt-sm-2">
                        <div class="col-6">
                            <h4 class="h5">Instagram</h4>
                        </div>
                        <div class="col-6 text-right">9.7 M Followers</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 9.7%" aria-valuenow="9.7" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End SOCIAL NETWORK -->
@endsection