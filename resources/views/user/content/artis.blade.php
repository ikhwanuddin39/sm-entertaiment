@extends('user.user')

@section('content')
    <!-- Start Banner Hero -->
    <section class="bg-light w-100">
        <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-7 text-start">
                    <h1 class="h2 py-5 text-primary typo-space-line">ARTIST</h1>
                    <p class="light-300">
                        Introduction of the top celebrities of SM Entertainment, from artists, actors, musical actors, entertainers, models, to sports stars.
                    </p>
                </div>
                <div class="col-lg-4">
                    <img src="{{asset('/assets/img/4588428-removebg-preview.png')}}" style="width: 500px;height:350px;">
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Hero -->

    <!-- Start Actris -->
    <section class="service-wrapper py-3">
        <div class="service-tag py-5 bg-secondary">
            <div class="col-md-12">
                <ul class="nav d-flex justify-content-center">
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary active shadow rounded-pill text-light px-4 light-300" href="#" data-filter=".project">All</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".1">Singer</a>
                    </li>
                    <li class="filter-btn nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".2">Actor</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".3">Entertainer</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".4">Model</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".5">Athlete</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section class="container overflow-hidden py-5">
        <div class="row gx-5 gx-sm-3 gx-lg-5 gy-lg-5 gy-3 pb-3 projects">
            @foreach($artis as $key=>$artiss)
                <!-- Start Recent Work -->
                <div class="col-xl-3 col-md-4 col-sm-6 project {{$artiss->kategori1}} {{$artiss->kategori2}} {{$artiss->kategori3}}">
                    <a href="{{$artiss->link}}" class="service-work card border-0 text-white shadow-sm overflow-hidden mx-5 m-sm-0">
                        <img class="service card-img" src="{{$artiss->gambar}}" alt="Card image">
                        <div class="service-work-vertical card-img-overlay d-flex align-items-end">
                            <div class="service-work-content text-left text-light">
                                <span class="btn btn-outline-light rounded-pill mb-lg-3 px-lg-4 light-300">
                                    @foreach ($kategori_artis as $key=>$kategori_artiss)
                                        @if($kategori_artiss->id== $artiss->kategori1)
                                            {{ $kategori_artiss->nama_kategori_actris }}, 
                                        @endif 
                                    @endforeach
                                    @foreach ($kategori_artis as $key=>$kategori_artiss)
                                        @if($kategori_artiss->id== $artiss->kategori2)
                                            {{ $kategori_artiss->nama_kategori_actris }}, 
                                        @endif 
                                    @endforeach
                                    @foreach ($kategori_artis as $key=>$kategori_artiss)
                                        @if($kategori_artiss->id== $artiss->kategori3)
                                            {{ $kategori_artiss->nama_kategori_actris }} 
                                        @endif 
                                    @endforeach
                                </span>
                                <p class="card-text">{{$artiss->nama_panjang}}</p>
                            </div>
                        </div>
                    </a>
                </div><!-- End Recent Work -->
            @endforeach
        </div>
    </section>
    <!-- End Actris -->
@endsection