@extends('user.user')

@section('content')
    <!-- Start Banner Hero -->
    <section class="bg-light w-100">
        <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-7 text-start">
                    <h1 class="h2 py-5 text-primary typo-space-line">ALBUM</h1>
                    <p class="light-300">
                        Introducing various types of music by SM Entertainment's top musicians
                    </p>
                </div>
                <div class="col-lg-4">
                    <img src="{{asset('/assets/img/20944455-removebg-preview.png')}}" style="width: 500px;height:350px;">
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Hero -->

    <!-- Start Recent Work -->
    <section class="py-5 mb-5">
        <div class="container">
            <div class="row gy-5 g-lg-5 mb-4">
                @foreach($album as $key=>$albums)
                    <!-- Start Recent Work -->
                    <div class="col-md-4 mb-3">
                        <a href="{{$albums->link}}" class="recent-work card border-0 shadow-lg overflow-hidden">
                            <img class="recent-work-img card-img" src="{{$albums->gambar}}" alt="Card image">
                            <div class="recent-work-vertical card-img-overlay d-flex align-items-end">
                                <div class="recent-work-content text-start mb-3 ml-3 text-dark">
                                    <h3 class="card-title light-300">{{$albums->nama_album}}</h3>
                                    <p class="card-text">{{$albums->nama_penyanyi}}</p>
                                    <p class="card-text">{{$albums->release}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- End Recent Work -->
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Recent Work -->
@endsection