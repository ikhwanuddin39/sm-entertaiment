@extends('user.user')

@section('content')
    <!-- Start Team Member -->
    <section class="container py-5">
        <h2 class="h2 text-black text-center py-5 typo-space-line-center">VARIETY SHOW</h2>
        <p class="text-muted text-center light-300">
            SM C&C is the largest variety show content creator in Korea.
            SM C&C established a stable movie and drama production system by using a distinguished management system and network that includes actors who are capable of displaying a broad range of performance skills. SM C&C has developed an environment for free and creative work by recruiting talented writers.
        </p>
        <div class="pt-5 pb-3 d-lg-flex align-items-center gx-5">
            <div class="col-lg-12 row">
                @foreach($variety as $key=>$varietys)
                    <div class="team-member col-md-4">
                        <img class="team-member-img img-fluid rounded-circle p-4" src="{{$varietys->gambar}}" alt="Card image">
                        <ul class="team-member-caption list-unstyled text-center pt-6 text-muted light-300">
                            <li>{{$varietys->penayangan}}</li>
                            <li>{{$varietys->program}}</li>
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Team Member -->
@endsection