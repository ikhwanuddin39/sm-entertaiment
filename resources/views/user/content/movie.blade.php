@extends('user.user')

@section('content')
    <!-- Start movie -->
    <section class="container py-5">
        <h2 class="h2 text-black text-center py-5 typo-space-line-center">Movie / Drama</h2>
        <p class="text-muted text-center light-300">
            We intend to grow into a global visual media content creator that produces major domestic and overseas TV drama series’ and films, and also co-produces and invests in joint overseas projects.  
        </p>
        <div class="row projects gx-lg-5">
            @foreach($movie as $key=>$movies)
                <a href="work-single.html" class="col-sm-6 col-lg-4 text-decoration-none project marketing social business">
                    <div class="service-work overflow-hidden card mb-5 mx-5 m-sm-0">
                        <img class="card-img-top" src="{{$movies->gambar}}" alt="...">
                        <div class="card-body">
                            <h5 class="card-title light-300 text-dark">{{$movies->penayangan}}</h5>
                            <p class="card-text light-300 text-dark">
                                {{$movies->program}}
                            </p>
                        </div>
                    </div>
                    <br>
                </a>
            @endforeach
        </div>
    </section>
    <!-- End movie -->
@endsection