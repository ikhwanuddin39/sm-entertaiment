@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Artist</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">Artist</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        List Artist
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <a href="/admin/actris/add_artist" class="btn-wide btn btn-success">
                                    <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                    Add List Artist
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <form method="GET" action="/admin/actris/cari">
                            <div class="row">
                                <div class="col-md-4 bottom-10">
                                    <input type="text" class="form-control" id="cari" name="cari" placeholder="Nama Panjang" value="{{ old('cari') }}">
                                </div>
                                <div class="col-md-5 bottom-10">
                                    <input type="submit" value="Search" class="btn btn-primary"/>
                                </div>
                            </div>
                            <!-- .row -->
                        </form>
                    </div>
                    <br>
                    @if(session('sukses'))
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{session('sukses')}}
                            </div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Nama</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($artis as $key=>$artiss)
                                <tr>
                                    <td class="text-center text-muted">{{$key+1}}</td>
                                    <td>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-3">
                                                    <div class="widget-content-left">
                                                        <img width="40" class="rounded-circle" src="{{$artiss->gambar}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="widget-content-left flex2">
                                                    <div class="widget-heading">{{$artiss->nama_panjang}}</div>
                                                    <div class="widget-subheading opacity-7">
                                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                                            @if($kategori_artiss->id== $artiss->kategori1)
                                                                {{ $kategori_artiss->nama_kategori_actris }}, 
                                                            @endif 
                                                        @endforeach
                                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                                            @if($kategori_artiss->id== $artiss->kategori2)
                                                                {{ $kategori_artiss->nama_kategori_actris }}, 
                                                            @endif 
                                                        @endforeach
                                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                                            @if($kategori_artiss->id== $artiss->kategori3)
                                                                {{ $kategori_artiss->nama_kategori_actris }} 
                                                            @endif 
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">{{$artiss->alamat}}</td>
                                    <td class="text-center">
                                        <a href="/admin/actris/{{$artiss->id}}/edit" class="mr-2 btn-icon btn-icon-only btn btn-outline-warning">
                                            <i class="pe-7s-note btn-icon-wrapper"> </i>
                                            Edit
                                        </a>
                                        <form action="/admin/actris/{{$artiss->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="delete" class="mr-2 btn-icon btn-icon-only btn btn-outline-danger">
                                        </form> 
                                        <a href="/admin/actris/{{$artiss->id}}" class="mr-2 btn-icon btn-icon-only btn btn-outline-info">
                                            <i class="pe-7s-look btn-icon-wrapper"> </i>
                                            Details 
                                        </a> 
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4" align="center">No Post</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="d-block text-center card-footer"></div>
                </div>
            </div>
        </div>
    </div>
@endsection