@extends('admin.admin')

@section('content')
<div class="app-main__inner">  
    <div class="row">
        <div class="col-md-12">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h4 class="m-0">Edit artist</h4>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">
                                    <a href="/admin/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/admin/actris">Artist</a>
                                </li>
                                <li class="breadcrumb-item active">Edit Artist</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <div class="main-card mb-3 card">
                <div class="card-header">
                    Edit Artist
                </div>
                <div class="card-body">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/actris/{{$artis->id}}">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="L_nama_panjang" class="">Nama Lengkap</label>
                                    @error('nama_panjang')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input name="nama_panjang" id="nama_panjang" placeholder="Ermadani Dyah Rachmawati" type="text" class="form-control" value="{{old('nama_panjang',$artis->nama_panjang)}} ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="L_nama_panggilan" class="">Nama Panggilan</label>
                                    @error('nama_panggilan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input name="nama_panggilan" id="nama_panggilan" placeholder="Dyah" type="text" class="form-control" value="{{old('nama_panggilan',$artis->nama_panggilan)}} ">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="L_tempat_lahir" class="">Tempat Lahir</label>
                                    @error('tempat_lahir')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input name="tempat_lahir" id="tempat_lahir" placeholder="Jombang" type="text" class="form-control" value="{{old('tempat_lahir',$artis->tempat_lahir)}} ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="L_tanggal_lahir" class="">Tanggal Lahir</label>
                                    @error('tanggal_lahir')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input name="tanggal_lahir" id="tanggal_lahir"  type="date" class="form-control" value="{{old('tanggal_lahir',$artis->tanggal_lahir)}} ">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="L_email" class="">Email</label>
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input name="email" id="email"  type="email" class="form-control" value="{{old('email',$artis->email)}} ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="L_no_hp" class="">No Hp</label>
                                    @error('no_hp')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input name="no_hp" id="no_hp" type="number" class="form-control" value="{{old('no_hp',$artis->no_hp)}} ">
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <label for="L_alamat" class="">Alamat</label>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input name="alamat" id="alamat"  type="text" class="form-control" value="{{old('alamat',$artis->alamat)}} ">
                        </div>
                        <div class="position-relative form-group">
                            <label for="L_gambar" class="">Gambar</label>
                            @error('gambar')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input name="gambar" id="gambar"  type="text" class="form-control" value="{{old('gambar',$artis->gambar)}} ">
                        </div>
                        <div class="position-relative form-group">
                            <label for="L_link" class="">Link</label>
                            @error('link')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input name="link" id="link"  type="text" class="form-control" value="{{old('link',$artis->link)}} ">
                        </div>
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="exampleEmail11" class="">Kategori 1</label>
                                    <select class="mb-2 form-control" name="kategori1">
                                        @error('kategori1')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <option value="">-- Pilih Kategori --</option>
                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                            <option value="{{ $kategori_artiss->id }}" @if($kategori_artiss->id == $artis->kategori1) selected @endif>{{ $kategori_artiss->nama_kategori_actris }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="exampleEmail11" class="">Kategori 2</label>
                                    <select class="mb-2 form-control" name="kategori2">
                                        <option value="">-- Pilih Kategori --</option>
                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                            <option value="{{ $kategori_artiss->id }}" @if($kategori_artiss->id== $artis->kategori2) selected @endif>{{ $kategori_artiss->nama_kategori_actris }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="exampleEmail11" class="">Kategori 3</label>
                                    <select class="mb-2 form-control" name="kategori3">
                                        <option value="">-- Pilih Kategori --</option>
                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                            <option value="{{ $kategori_artiss->id }}" @if($kategori_artiss->id== $artis->kategori3) selected @endif>{{ $kategori_artiss->nama_kategori_actris }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-block text-center card-footer">
                            <a href="/admin/actris" class="btn btn-warning float-left">
                                <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                Back
                            </a>
                            <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                <i class="pe-7s-pen btn-icon-wrapper"> </i>
                                Edit 
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection