@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Edit User</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/user">User</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit User</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Edit User
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/user/{{$user->id}}">
                            @csrf
                            @method('PUT')
                            <div class="position-relative form-group">
                                <label for="L_nama" class="">Nama Lengkap</label>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="name" id="name" placeholder="Ermadani Dyah Rachmawati" type="text" class="form-control" value="{{old('name',$user->name)}} ">
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_email" class="">Email</label>
                                        @error('email')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="email" id="email" type="email" class="form-control" value="{{old('email',$user->email)}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_password" class="">Password</label>
                                        @error('password')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="password" id="password" type="password" class="form-control" value="{{old('password',$user->password)}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="/admin/user" class="btn btn-warning float-left">
                                    <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                    Back
                                </a>
                                <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                    <i class="pe-7s-pen btn-icon-wrapper"> </i>
                                    Edit 
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection