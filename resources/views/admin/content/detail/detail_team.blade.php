@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Details Team</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/team">Team</a>
                                    </li>
                                    <li class="breadcrumb-item active">Details Team</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Details Team
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>  
                                <tr>
                                    <td colspan="2">
                                        <i class="mr-3 fas fa-users"></i>
                                        <strong>Detail Team</strong>
                                    </td>
                                </tr>               
                                <tr>
                                    <td width="20%">
                                        <strong>Nama Lengkap</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->nama_lengkap}}
                                    </td>
                                </tr>                 
                                <tr>
                                    <td width="20%">
                                        <strong>Nama Panggilan</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->nama_panggilan}}
                                    </td>
                                </tr>                 
                                <tr>
                                    <td width="20%">
                                        <strong>Tempat Lahir</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->tempat_lahir}}
                                    </td>
                                </tr> 
                                <tr>
                                    <td width="20%">
                                        <strong>Tanggal Lahir</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->tanggal_lahir}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Email</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->email}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>No Hp</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->no_hp}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Alamat</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->alamat}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Kategori</strong>
                                    </td>
                                    <td width="80%">
                                        @foreach ($kategori_team as $key=>$kategori_teams)
                                            @if($kategori_teams->id== $team->kategori_team)
                                                {{ $kategori_teams->nama_kategori }}
                                            @endif 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Jabatan</strong>
                                    </td>
                                    <td width="80%">
                                        {{$team->jabatan}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Foto</strong>
                                    </td>
                                    <td width="80%">
                                        <img width="300"  src="{{$team->gambar}}" alt="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-block text-center card-footer">
                        <a href="/admin/team" class="btn btn-warning float-right">
                            <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection