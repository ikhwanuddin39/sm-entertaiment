@extends('admin.admin')

@section('content')
<div class="app-main__inner">  
    <div class="row">
        <div class="col-md-12">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h4 class="m-0">Album</h4>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">
                                    <a href="admin/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Album</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <div class="main-card mb-3 card">
                <div class="card-header">
                    List Album
                    <div class="btn-actions-pane-right">
                        <div role="group" class="btn-group-sm btn-group">
                            <a href="/admin/album/add_album" class="btn-wide btn btn-success">
                                <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                Add List Album
                            </a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <form method="GET" action="/admin/album/cari">
                        <div class="row">
                            <div class="col-md-4 bottom-10">
                                <input type="text" class="form-control" id="cari" name="cari" placeholder="Nama Album" value="{{ old('cari') }}">
                            </div>
                            <div class="col-md-5 bottom-10">
                                <input type="submit" value="Search" class="btn btn-primary"/>
                            </div>
                        </div>
                        <!-- .row -->
                    </form>
                </div>
                <br>
                @if(session('sukses'))
                    <div class="col-sm-12">
                        <div class="alert alert-success">
                            {{session('sukses')}}
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Album</th>
                                <th class="text-center">Penyanyi</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($album as $key=>$albums)
                                <tr>
                                    <td class="text-center text-muted">{{$key+1}}</td>
                                    <td class="text-center">{{$albums->nama_album}}</td>
                                    <td class="text-center">{{$albums->nama_penyanyi}}</td>
                                    <td class="text-center">{{$albums->release}}</td>
                                    <td class="text-center">
                                        <a href="/admin/album/{{$albums->id}}/edit" class="mr-2 btn-icon btn-icon-only btn btn-outline-warning">
                                            <i class="pe-7s-note btn-icon-wrapper"> </i>
                                            Edit
                                        </a>
                                        <form action="/admin/album/{{$albums->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="delete" class="mr-2 btn-icon btn-icon-only btn btn-outline-danger">
                                        </form> 
                                        <a href="/admin/album/{{$albums->id}}" class="mr-2 btn-icon btn-icon-only btn btn-outline-info">
                                            <i class="pe-7s-look btn-icon-wrapper"> </i>
                                            Details 
                                        </a> 
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5" align="center">No Post</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-block text-center card-footer"></div>
            </div>
        </div>
    </div>
</div>
@endsection