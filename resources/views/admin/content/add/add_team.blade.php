@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Add Team</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/team">Team</a>
                                    </li>
                                    <li class="breadcrumb-item active">Add Team</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Add Team
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/team">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_nama_panjang" class="">Nama Lengkap</label>
                                        @error('nama_lengkap')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="nama_lengkap" id="nama_lengkap" placeholder="Ermadani Dyah Rachmawati" type="text" class="form-control" value="{{old('nama_lengkap','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_nama_panggilan" class="">Nama Panggilan</label>
                                        @error('nama_panggilan')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="nama_panggilan" id="nama_panggilan" placeholder="Dyah" type="text" class="form-control" value="{{old('nama_panggilan','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_tempat_lahir" class="">Tempat Lahir</label>
                                        @error('tempat_lahir')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="tempat_lahir" id="tempat_lahir" placeholder="Jombang" type="text" class="form-control" value="{{old('tempat_lahir','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_tanggal_lahir" class="">Tanggal Lahir</label>
                                        @error('tanggal_lahir')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="tanggal_lahir" id="tanggal_lahir"  type="date" class="form-control" value="{{old('tanggal_lahir','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_email" class="">Email</label>
                                        @error('email')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="email" id="email"  type="email" class="form-control" value="{{old('email','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_no_hp" class="">No Hp</label>
                                        @error('no_hp')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="no_hp" id="no_hp" type="number" class="form-control" value="{{old('no_hp','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative form-group">
                                <label for="L_alamat" class="">Alamat</label>
                                @error('alamat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="alamat" id="alamat"  type="text" class="form-control" value="{{old('alamat','')}} ">
                            </div>
                            <div class="position-relative form-group">
                                <label for="L_gambar" class="">Gambar</label>
                                @error('gambar')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="gambar" id="gambar"  type="text" class="form-control" value="{{old('gambar','')}} ">
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_kategori_team" class="">Kategori Team</label>
                                        <select class="mb-2 form-control" name="kategori_team">
                                            @error('kategori_team')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <option value="">-- Pilih Kategori --</option>
                                            @foreach ($kategori_team as $key=>$kategori_teams)
                                                <option value="{{ $kategori_teams->id }}">{{ $kategori_teams->nama_kategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_jabatan" class="">Jabatan</label>
                                        @error('jabatan')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="jabatan" id="jabatan" type="text" class="form-control" value="{{old('jabatan','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="/admin/team" class="btn btn-warning float-left">
                                    <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                    Back
                                </a>
                                <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                    <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                    Add 
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection