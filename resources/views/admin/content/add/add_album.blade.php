@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Add Album</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/album">Album</a>
                                    </li>
                                    <li class="breadcrumb-item active">Add Album</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Add Album
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/album">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_nama_album" class="">Album</label>
                                        @error('nama_album')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="nama_album" id="nama_album"  type="text" class="form-control" value="{{old('nama_album','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_nama_penyanyi" class="">Penyanyi</label>
                                        @error('nama_penyanyi')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="nama_penyanyi" id="nama_penyanyi"  type="text" class="form-control" value="{{old('nama_penyanyi','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_album_type" class="">Album Type</label>
                                        @error('album_type')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="album_type" id="album_type" type="text" class="form-control" value="{{old('album_type','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_genre" class="">Genre</label>
                                        @error('genre')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="genre" id="genre" type="text" class="form-control" value="{{old('genre','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_record_label" class="">Record Label</label>
                                        @error('record_label')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="record_label" id="record_label" type="text" class="form-control" value="{{old('record_label','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_distribution" class="">Distribution</label>
                                        @error('distribution')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="distribution" id="distribution" type="text" class="form-control" value="{{old('distribution','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative form-group">
                                <label for="L_release" class="">Release</label>
                                @error('release')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="release" id="release" type="date" class="form-control" value="{{old('release','')}} ">
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_gambar" class="">Gambar</label>
                                        @error('gambar')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="gambar" id="gambar" type="text" class="form-control" value="{{old('gambar','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_link" class="">Link</label>
                                        @error('link')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="link" id="link" type="text" class="form-control" value="{{old('link','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="/admin/album" class="btn btn-warning float-left">
                                    <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                    Back
                                </a>
                                <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                    <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                    Add 
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection