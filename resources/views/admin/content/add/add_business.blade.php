@extends('admin.admin')

@section('content')
<div class="app-main__inner">  
    <div class="row">
        <div class="col-md-12">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h4 class="m-0">Add Business Unit</h4>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">
                                    <a href="/admin/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/admin/business">Business Unit</a>
                                </li>
                                <li class="breadcrumb-item active">Add Business Unit</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <div class="main-card mb-3 card">
                <div class="card-header">
                    Add Business Unit
                </div>
                <div class="card-body">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/business">
                        @csrf
                            <div class="position-relative form-group">
                                <label for="L_nama_business" class="">Nama</label>
                                @error('nama_business')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="nama_business" id="nama_business"  type="text" class="form-control" value="{{old('nama_business','')}} ">
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_jenis_business" class="">Jenis</label>
                                        @error('jenis_business')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="jenis_business" id="jenis_business"  type="text" class="form-control" value="{{old('jenis_business','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="exampleEmail11" class="">Kategori Business</label>
                                        <select class="mb-2 form-control" name="kategori_business">
                                            @error('kategori_business')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <option value="">-- Pilih Kategori --</option>
                                            @foreach ($kategori_business as $key=>$kategori_businesss)
                                                <option value="{{ $kategori_businesss->id }}">{{ $kategori_businesss->nama_kategori_business }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative form-group">
                                <label for="hobi" class="">Gambar</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="gambar" id="gambar">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>  
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="/admin/business" class="btn btn-warning float-left">
                                    <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                    Back
                                </a>
                                <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                    <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                    Add 
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection