<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKategoriActrisToActris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actris', function (Blueprint $table) {
            $table->foreign('kategori2')->references('id')->on('kategori_actris');
            $table->foreign('kategori1')->references('id')->on('kategori_actris');
            $table->foreign('kategori3')->references('id')->on('kategori_actris');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actris', function (Blueprint $table) {
            $table->dropForeign(['kategori1']);
            $table->dropForeign(['kategori2']);
            $table->dropForeign(['kategori3']);
        });
    }
}
