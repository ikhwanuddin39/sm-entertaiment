<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actris', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_panggilan');
            $table->string('nama_panjang');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('email');
            $table->integer('no_hp');
            $table->string('alamat');
            $table->text('gambar');
            $table->text('link');
            $table->unsignedBigInteger('kategori1');
            $table->unsignedBigInteger('kategori2');
            $table->unsignedBigInteger('kategori3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actris');
    }
}
