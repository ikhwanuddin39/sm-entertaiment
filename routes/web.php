<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//halaman user
Route::get('/user', function () {
    return view('user.user');
});

// Route::get('/', function () {
//     return view('user.content.home');
// });

// Route::get('/artis', function () {
//     return view('user.content.artis');
// });

// Route::get('/album', function () {
//     return view('user.content.album');
// });

// Route::get('/variety', function () {
//     return view('user.content.variety');
// });

// Route::get('/movie', function () {
//     return view('user.content.movie');
// });

//---halaman admin---
Route::get('/admin/admin', function () {
    return view('admin.admin');
});

// Route::get('/admin/dashboard', function () {
//     return view('admin.content.dashboard');
// });

// Route::get('/admin/team', function () {
//     return view('admin.content.team');
// });

// Route::get('/admin/companies', function () {
//     return view('admin.content.companies');
// });

// Route::get('/admin/business', function () {
//     return view('admin.content.business');
// });

// Route::get('/admin/actris', function () {
//     return view('admin.content.actris');
// });

// Route::get('/admin/album', function () {
//     return view('admin.content.album');
// });

// Route::get('/admin/variety', function () {
//     return view('admin.content.variety');
// });

// Route::get('/admin/movie', function () {
//     return view('admin.content.movie');
// });

// Route::get('/admin/user', function () {
//     return view('admin.content.user');
// });

//---->halaman aksinya<-----

//---->artis<----
// Route::get('/admin/detail_artist', function () {
//     return view('admin.content.detail.detail_artist');
// });

// Route::get('/admin/add_artist', function () {
//     return view('admin.content.add.add_artist');
// });

// Route::get('/admin/edit_artist', function () {
//     return view('admin.content.edit.edit_artist');
// });

//--->album<---
// Route::get('/admin/detail_album', function () {
//     return view('admin.content.detail.detail_album');
// });

// Route::get('/admin/add_album', function () {
//     return view('admin.content.add.add_album');
// });

// Route::get('/admin/edit_album', function () {
//     return view('admin.content.edit.edit_album');
// });

//--->business<---
// Route::get('/admin/detail_business', function () {
//     return view('admin.content.detail.detail_business');
// });

// Route::get('/admin/add_business', function () {
//     return view('admin.content.add.add_business');
// });

// Route::get('/admin/edit_business', function () {
//     return view('admin.content.edit.edit_business');
// });

//--->companies<---
// Route::get('/admin/detail_companies', function () {
//     return view('admin.content.detail.detail_companies');
// });

// Route::get('/admin/add_companies', function () {
//     return view('admin.content.add.add_companies');
// });

// Route::get('/admin/edit_companies', function () {
//     return view('admin.content.edit.edit_companies');
// });

//--->movie<---
// Route::get('/admin/detail_movie', function () {
//     return view('admin.content.detail.detail_movie');
// });

// Route::get('/admin/add_movie', function () {
//     return view('admin.content.add.add_movie');
// });

// Route::get('/admin/edit_movie', function () {
//     return view('admin.content.edit.edit_movie');
// });

//--->team<---
// Route::get('/admin/detail_team', function () {
//     return view('admin.content.detail.detail_team');
// });

// Route::get('/admin/add_team', function () {
//     return view('admin.content.add.add_team');
// });

// Route::get('/admin/edit_team', function () {
//     return view('admin.content.edit.edit_team');
// });

//--->user<---
// Route::get('/admin/detail_user', function () {
//     return view('admin.content.detail.detail_user');
// });

// Route::get('/admin/add_user', function () {
//     return view('admin.content.add.add_user');
// });

// Route::get('/admin/edit_user', function () {
//     return view('admin.content.edit.edit_user');
// });


//--->variety<---
// Route::get('/admin/detail_variety', function () {
//     return view('admin.content.detail.detail_variety');
// });

// Route::get('/admin/add_variety', function () {
//     return view('admin.content.add.add_variety');
// });

// Route::get('/admin/edit_variety', function () {
//     return view('admin.content.edit.edit_variety');
// });

//---Halaman Controller---//

//---Halaman Admin---//

//---->Actris<----//
Route::get('/admin/actris/add_artist','ActrisController@tambah');
Route::post('/admin/actris','ActrisController@store');
Route::get('/admin/actris','ActrisController@index');
Route::get('/admin/actris/cari','ActrisController@cari');
Route::get('/admin/actris/{artist_id}','ActrisController@detail');
Route::get('/admin/actris/{artist_id}/edit','ActrisController@edit');
Route::put('/admin/actris/{artist_id}','ActrisController@update');
Route::delete('/admin/actris/{artist_id}','ActrisController@delate');

//---->Album<----//
Route::get('/admin/album/add_album','AlbumController@tambah');
Route::post('/admin/album','AlbumController@store');
Route::get('/admin/album','AlbumController@index');
Route::get('/admin/album/cari','AlbumController@cari');
Route::get('/admin/album/{album_id}','AlbumController@detail');
Route::get('/admin/album/{album_id}/edit','AlbumController@edit');
Route::put('/admin/album/{album_id}','AlbumController@update');
Route::delete('/admin/album/{album_id}','AlbumController@delate');

//---->Variety Show<----//
Route::get('/admin/variety/add_variety','VarietyController@tambah');
Route::post('/admin/variety','VarietyController@store');
Route::get('/admin/variety','VarietyController@index');
Route::get('/admin/variety/cari','VarietyController@cari');
Route::get('/admin/variety/{variety_id}/edit','VarietyController@edit');
Route::put('/admin/variety/{variety_id}','VarietyController@update');
Route::delete('/admin/variety/{variety_id}','VarietyController@delate');

//---->Movie/drama<----//
Route::get('/admin/movie/add_movie','MovieController@tambah');
Route::post('/admin/movie','MovieController@store');
Route::get('/admin/movie','MovieController@index');
Route::get('/admin/movie/cari','MovieController@cari');
Route::get('/admin/movie/{movie_id}/edit','MovieController@edit');
Route::put('/admin/movie/{movie_id}','MovieController@update');
Route::delete('/admin/movie/{movie_id}','MovieController@delate');

//---->Team<----//
Route::get('/admin/team/add_team','TeamController@tambah');
Route::post('/admin/team','TeamController@store');
Route::get('/admin/team','TeamController@index');
Route::get('/admin/team/cari','TeamController@cari');
Route::get('/admin/team/{team_id}','TeamController@detail');
Route::get('/admin/team/{team_id}/edit','TeamController@edit');
Route::put('/admin/team/{team_id}','TeamController@update');
Route::delete('/admin/team/{team_id}','TeamController@delate');

//---->Companies<----//
Route::get('/admin/companies/add_companies','CompaniesController@tambah');
Route::post('/admin/companies','CompaniesController@store');
Route::get('/admin/companies','CompaniesController@index');
Route::get('/admin/companies/cari','CompaniesController@cari');
Route::get('/admin/companies/{companies_id}','CompaniesController@detail');
Route::get('/admin/companies/{companies_id}/edit','CompaniesController@edit');
Route::put('/admin/companies/{companies_id}','CompaniesController@update');
Route::delete('/admin/companies/{companies_id}','CompaniesController@delate');

//---->Business<----//
Route::get('/admin/business/add_business','BusinessController@tambah');
Route::post('/admin/business','BusinessController@store');
Route::get('/admin/business','BusinessController@index');
Route::get('/admin/business/cari','BusinessController@cari');
Route::get('/admin/business/{business_id}/edit','BusinessController@edit');
Route::put('/admin/business/{business_id}','BusinessController@update');
Route::delete('/admin/business/{business_id}','BusinessController@delate');

//---->User<----//
Route::get('/admin/user/add_user','UserController@tambah');
Route::post('/admin/user','UserController@store');
Route::get('/admin/user','UserController@index');
Route::get('/admin/user/cari','UserController@cari');
Route::get('/admin/user/{user_id}/edit','UserController@edit');
Route::put('/admin/user/{user_id}','UserController@update');
Route::delete('/admin/user/{user_id}','UserController@delate');

//---->Dashboard<----//
Route::get('/admin/dashboard','HalamanAdminController@count_all')->name('halaman_admin');

//---Halaman User---//
Route::get('/artis','HomeController@index');
Route::get('/album','HomeController@index2');
Route::get('/variety','HomeController@index3');
Route::get('/movie','HomeController@index4');
Route::get('/','HomeController@index5');

//---Halaman Login---//

Route::get('/register','AuthController@getRegister')->name('register');
Route::post('/register','AuthController@postRegister');
Route::get('/login','AuthController@getLogin')->name('login');
Route::post('/login','AuthController@postLogin');
Route::get('/logout','AuthController@logout')->name('logout');
