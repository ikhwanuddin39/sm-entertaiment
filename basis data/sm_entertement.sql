-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 05, 2021 at 02:33 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sm_entertement`
--

-- --------------------------------------------------------

--
-- Table structure for table `actris`
--

CREATE TABLE `actris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_panggilan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_panjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` int(11) NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori1` bigint(20) UNSIGNED NOT NULL,
  `kategori2` bigint(20) UNSIGNED NOT NULL,
  `kategori3` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actris`
--

INSERT INTO `actris` (`id`, `nama_panggilan`, `nama_panjang`, `tempat_lahir`, `tanggal_lahir`, `email`, `no_hp`, `alamat`, `gambar`, `link`, `kategori1`, `kategori2`, `kategori3`, `created_at`, `updated_at`) VALUES
(1, 'Kangta', 'Kangta', 'Soul', '2021-06-13', 'kangta@gmail', 1223, 'soul', 'https://smentcorporation.s3.amazonaws.com/upload/thumb/artist/2021/03/30/upCArLnimSf09d4648bc084f9592c4bb205eb374daYUtOqZMcP1zocdhpxd.p5y.jpg', 'https://www.smentertainment.com/Entertainment/ArtistProfile/301?Code=&Page=', 4, 1, 5, NULL, NULL),
(7, 'BOA', 'BOA', 'Soul', '2021-06-20', 'BOA@gmail.com', 1234, 'Jombang', 'https://smentcorporation.s3.amazonaws.com/upload/thumb/artist/2021/01/13/5a2Ef0GIU9b36c3e09faf243898c3d11bd4c4dc5827e6Ynkmly82fvabes2.cae.jpg', 'https://www.smentertainment.com/Entertainment/ArtistProfile/302?Code=&Page=', 1, 1, 1, NULL, NULL),
(8, 'TVXQ!', 'TVXQ!', 'Soul', '2021-06-13', 'TVXQ@gmail.com', 1234, 'Jombang', 'https://smentcorporation.s3.amazonaws.com/upload/thumb/artist/2019/12/26/LJyNleHQ2Dd0422135fa464877a4ad2962279a8048GqdBcxFjVtcazaayto.f0i.jpg', 'https://www.smentertainment.com/Entertainment/ArtistProfile/303?Code=&Page=', 1, 2, 4, NULL, NULL),
(9, 'SUPER JUNIOR', 'SUPER JUNIOR', 'Soul', '2021-07-11', 'SUPER_JUNIOR@gmail.com', 123, 'Soul', 'https://smentcorporation.s3.amazonaws.com/upload/thumb/artist/2021/03/30/BUr8Cf5yQac455026260cd47a6b56606b02d743a12NtHjTlFhGx3fftquaq.pki.jpg', 'https://www.smentertainment.com/Entertainment/ArtistProfile/304?Code=&Page=', 1, 1, 4, '2021-07-02 03:10:23', '2021-07-02 04:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_album` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penyanyi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distribution` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release` date NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `nama_album`, `nama_penyanyi`, `album_type`, `genre`, `record_label`, `distribution`, `release`, `gambar`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Single ‘Back To You’', 'WayV-KUN&XIAOJUN', 'Single', 'Ballad', 'SM ENTERTAINMENT', 'ETC', '2021-06-14', 'https://socdn.smtown.com/upload/mf/album/images/000/011/613/11613.jpg', 'https://www.smentertainment.com/Entertainment/Albums/11613', NULL, NULL),
(2, 'iScreaM Vol.9 : 맛 (Hot Sauce) Remixes', 'NCT DREAM', 'Digital Single', 'ETC', 'SM ENTERTAINMENT', 'ETC', '2021-06-10', 'https://socdn.smtown.com/upload/mf/album/images/000/011/612/11612.jpg', 'https://www.smentertainment.com/Entertainment/Albums/11612', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_business` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_business` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_business` bigint(20) UNSIGNED NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `nama_business`, `jenis_business`, `kategori_business`, `gambar`, `created_at`, `updated_at`) VALUES
(3, 'Music Producing', 'BALJUNSO Label', 1, '1625203312_musik.png', NULL, NULL),
(4, 'Production', 'Movie TV(Drama / Variety Show) Musical', 1, '1625203385_Production.png', NULL, NULL),
(5, 'Concert', 'Dream Maker', 1, '1625203440_conceret.png', NULL, NULL),
(6, 'Management', 'Management', 1, '1625203487_management.png', NULL, NULL),
(7, 'Online Store', 'SMTOWN &STORE', 2, '1625203536_onlineStore.png', NULL, NULL),
(8, 'Restourant', 'SMT HOUSE', 2, '1625203567_restorant.png', NULL, NULL),
(9, 'Wine', 'TEMECULA EMOS,estate winery', 2, '1625203593_wine.png', NULL, NULL),
(10, 'The CELEBRITY', 'The CELEBRITY', 2, '1625203616_the celebrity.png', NULL, NULL),
(11, 'Travel Agency', 'BT&I,SM TRAVEL,Tour Express,HOTELTREES', 2, '1625203668_travel.png', NULL, NULL),
(12, 'Mobile Platform', 'Lysn (LBS/interest-base SNS),everysing (Karaoke App),Superstar SMTOWN (GAME),SMTOWN NOW (News),FanBook (Fan Art Social Platform)', 3, '1625203736_mobile.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_companies` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` int(11) NOT NULL,
  `ceo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `nama_companies`, `alamat`, `email`, `no_hp`, `ceo`, `gambar`, `created_at`, `updated_at`) VALUES
(4, 'SM C&C', 'Soul', 'SM_C&C@gmail.com', 12345, 'Lee Jeno', '1625203814_logo cc.png', NULL, NULL),
(5, 'Dream Maker Entertainment', 'Soul', 'Dream_Maker@gmail.com', 12345, 'Lee Taeyong', '1625203868_download-removebg-preview.png', NULL, NULL),
(6, 'SM True', 'Soul', 'SM_True@gmail.com', 12345, 'Lee Mark', '1625204018_xWi5ewdL-removebg-preview.png', NULL, NULL),
(7, 'MYSTIC STORY', 'Soul', 'MYSTIC_STORY@gmail.com', 12345, 'Lee Mark', '1625204089_x98FrzSM_400x400-removebg-preview.png', NULL, NULL),
(8, 'ESteem', 'Soul', 'ESteem@gmail.com', 1234, 'Lee Jeno', '1625204135_gHIh5dK9rR4e6d4e1f76b4483daca2688c4e4583eaMlXzTDFViY4exadqcp.vk2-removebg-preview.png', NULL, NULL),
(9, 'DEAR U', 'Jombang', 'DEAR_U@gmail.com', 1234, 'Lee Jeno', '1625204200_v7rWouiFZ683b735d13b504bfa89c8f5aba96c1aea83cy54mgSAp1rss4ab.f5y-removebg-preview.png', NULL, NULL),
(10, 'SM Life Design Group Co., Ltd', 'Soul', 'SM_Life_design_Group_Co@gmail.com', 12345, 'Lee Mark', '1625204279_lolo-removebg-preview.png', NULL, NULL),
(11, 'MILLION MARKET', 'Soul', 'MILLION_MARKET@gmail.com', 1234, 'Lee Mark', '1625204345_jj-removebg-preview.png', NULL, NULL),
(12, 'SM Japan', 'Soul', 'SM_Japan@gmail.com', 12345, 'Lee Mark', '1625204417_670ysoOSDw496b2dd59e3e4f778fe9354fbef529dcpQvRFqLME1u3ac31oc.w3g-removebg-preview.png', NULL, NULL),
(13, 'galaxiaSM', 'Soul', 'galaxiaSM@gmail.com', 12345, 'Lee Mark', '1625204474_bwkeXSuqNh74360cf2925a46ae918e13e38f4d76f8LnFjpaM49Q1is3nncp.14q-removebg-preview.png', NULL, NULL),
(14, 'KEYEAST', 'Soul', 'KEYEAST@gmail.com', 1234, 'Lee Mark', '1625204524_KeyEast_logo-removebg-preview.png', NULL, NULL),
(15, 'SMTOWN PLANNER', 'soul', 'SMTOWN_PLANNER@gmail.com', 12345, 'Lee Mark', '1625204603_3zfKGSXLT240dcc5f6a2f6480686b5335e3a77d33cVpqWk4s6wE45q0lo1q.lr3-removebg-preview.png', NULL, NULL),
(16, 'SM USA', 'Soul', 'SM_USA@gmail.com', 12345, 'Lee Mark', '1625204654_kgdG6FhIfDce9aa5822eb944cdb2bcab57086b1a75OpeSVnwW7ocoskqqnr.frq-removebg-preview.png', NULL, NULL),
(17, 'SM F&B Development', 'Soul', 'F&B_Development@gmail.com', 12345, 'Lee Mark', '1625204723_MBWYgkwIh27e8d3c95994c427a986b77a724d7dd35ytEps0mnCZjlwikhoc.dvc-removebg-preview.png', NULL, NULL),
(18, 'SM STORE', 'Soul', 'SM_STORE@GMAIL.COM', 123, 'Lee Jeno', '1625215766_store-removebg-preview.png', NULL, NULL),
(19, 'SM Brand Marketing', 'Soul', 'Brand_Marketing@gmail.com', 1234, 'Lee Mark', '1625204955_store2-removebg-preview.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_actris`
--

CREATE TABLE `kategori_actris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori_actris` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_actris`
--

INSERT INTO `kategori_actris` (`id`, `nama_kategori_actris`, `created_at`, `updated_at`) VALUES
(1, 'Singer', NULL, NULL),
(2, 'Actor', NULL, NULL),
(3, 'Entertainer', NULL, NULL),
(4, 'Model', NULL, NULL),
(5, 'Athlete', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_business`
--

CREATE TABLE `kategori_business` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori_business` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_business`
--

INSERT INTO `kategori_business` (`id`, `nama_kategori_business`, `created_at`, `updated_at`) VALUES
(1, 'Entertainment', NULL, NULL),
(2, 'Commerce & Amusement', NULL, NULL),
(3, 'Mobile Entertainment Platform', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_teams`
--

CREATE TABLE `kategori_teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_teams`
--

INSERT INTO `kategori_teams` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Inside Director', NULL, NULL),
(2, 'Outside Director', NULL, NULL),
(3, 'Auditor', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_27_034217_create_teams_table', 2),
(5, '2021_06_27_034302_create_kategori_teams_table', 2),
(6, '2021_06_27_034341_create_companies_table', 2),
(7, '2021_06_27_034433_create_business_table', 3),
(8, '2021_06_27_034524_create_kategori_business_table', 3),
(9, '2021_06_27_034559_create_actris_table', 3),
(10, '2021_06_27_034627_create_kategori_actris_table', 3),
(11, '2021_06_27_034655_create_album_table', 3),
(12, '2021_06_27_034723_create_variety_table', 3),
(13, '2021_06_27_034745_create_movie_table', 3),
(14, '2021_06_27_051132_add_kategori_team_to_teams', 4),
(15, '2021_06_27_052005_create_team_table', 4),
(16, '2021_06_27_052148_add_kategori_team_to_team', 4),
(17, '2021_06_27_052315_add_kategori_business_to_business', 5),
(18, '2021_06_27_052411_add_kategori_actris_to_actris', 5);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penayangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `gambar`, `program`, `penayangan`, `created_at`, `updated_at`) VALUES
(1, 'https://www.smentertainment.com/Images/business/entertainment/Business_ENTERTAINMENT_MOVIEDRAMA21.jpg', 'HUSH', 'JTBC', NULL, NULL),
(2, 'https://www.smentertainment.com/Images/business/entertainment/Business_ENTERTAINMENT_MOVIEDRAMA22.jpg', 'Liv On', 'JTBC', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_panggilan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` int(11) NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_team` bigint(20) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `nama_lengkap`, `nama_panggilan`, `tempat_lahir`, `tanggal_lahir`, `email`, `no_hp`, `alamat`, `gambar`, `kategori_team`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'Lee Sung Su', 'Sung Su', 'Soul', '2021-06-13', 'Lee_Sung_Su@gmail.com', 1234, 'Jombang', 'https://smentcorporation.s3.amazonaws.com/upload/thumb/artist/2017/09/25/4urdWoz9IZ682da13a0fbc42bba4e1b9e81bbe52bcK36TYP1GLysnkpinvh.vqw.jpg', 1, 'CEO', NULL, NULL),
(2, 'Tak, Young Jun', 'Young Jun', 'Soul', '2010-05-09', 'Tak_Young_Jun@gmail.com', 1234, 'Jombang', 'https://smentcorporation.s3.amazonaws.com/upload/thumb/artist/2017/09/25/4urdWoz9IZ682da13a0fbc42bba4e1b9e81bbe52bcK36TYP1GLysnkpinvh.vqw.jpg', 1, 'CMO', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ermadani dyah', 'ermadani.dyah09@gmail.com', NULL, '$2y$10$eEgpO2WJszrXYwpkPqxnCeTfJMYfb8qkO69TC.zsDlbZpD4fmNtfi', NULL, '2021-07-02 04:59:42', '2021-07-02 04:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `variety`
--

CREATE TABLE `variety` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penayangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variety`
--

INSERT INTO `variety` (`id`, `gambar`, `program`, `penayangan`, `created_at`, `updated_at`) VALUES
(1, 'https://www.smentertainment.com/Images/business/entertainment/0612_02_Heart_for_you.jpg', 'Heart for you', 'Naver TV /V LIVE', NULL, NULL),
(2, 'https://www.smentertainment.com/Images/business/entertainment/0612_04_Amazing_Saturday.jpg', 'Amazing Saturday', 'tvN', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actris`
--
ALTER TABLE `actris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actris_kategori2_foreign` (`kategori2`),
  ADD KEY `actris_kategori1_foreign` (`kategori1`),
  ADD KEY `actris_kategori3_foreign` (`kategori3`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_kategori_business_foreign` (`kategori_business`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_actris`
--
ALTER TABLE `kategori_actris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_business`
--
ALTER TABLE `kategori_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_teams`
--
ALTER TABLE `kategori_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_kategori_team_foreign` (`kategori_team`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variety`
--
ALTER TABLE `variety`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actris`
--
ALTER TABLE `actris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_actris`
--
ALTER TABLE `kategori_actris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori_business`
--
ALTER TABLE `kategori_business`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kategori_teams`
--
ALTER TABLE `kategori_teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `variety`
--
ALTER TABLE `variety`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actris`
--
ALTER TABLE `actris`
  ADD CONSTRAINT `actris_kategori1_foreign` FOREIGN KEY (`kategori1`) REFERENCES `kategori_actris` (`id`),
  ADD CONSTRAINT `actris_kategori2_foreign` FOREIGN KEY (`kategori2`) REFERENCES `kategori_actris` (`id`),
  ADD CONSTRAINT `actris_kategori3_foreign` FOREIGN KEY (`kategori3`) REFERENCES `kategori_actris` (`id`);

--
-- Constraints for table `business`
--
ALTER TABLE `business`
  ADD CONSTRAINT `business_kategori_business_foreign` FOREIGN KEY (`kategori_business`) REFERENCES `kategori_business` (`id`);

--
-- Constraints for table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `team_kategori_team_foreign` FOREIGN KEY (`kategori_team`) REFERENCES `kategori_teams` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
