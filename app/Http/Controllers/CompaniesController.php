<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\companies;
class CompaniesController extends Controller
{
    public function index(){
        // $companies=DB::table('companies')->get();
        $companies=companies::all();
        return view('admin.content.companies',compact('companies'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $companies=DB::table('companies')->where('nama_companies','like',"%".$cari."%")->get();
        $companies=companies::where('nama_companies','like',"%".$cari."%")->get();
        // mengirim data pegawai ke view index
        return view('admin.content.companies',compact('companies'));
	}
    public function tambah(){
        return view('admin.content.add.add_companies');
    }
    public function store(Request $request){
        $file = $request->file('gambar');
        
        // Mendapatkan Nama File
        $nama_file = time()."_".$file->getClientOriginalName();
    
        // Mendapatkan Extension File
        $extension = $file->getClientOriginalExtension();
    
        // Mendapatkan Ukuran File
        $ukuran_file = $file->getSize();
    
        // Proses Upload File
        $destinationPath = 'uploads';
        $file->move($destinationPath,$nama_file);
        $request->validate([
            'nama_companies'=>'required|unique:companies',
            'alamat'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'ceo'=>'required',
        ]);
        // $query=DB::table('companies')->insert(
        //     [
        //         "gambar"=>$nama_file,
        //         "nama_companies"=>$request['nama_companies'],
        //         "alamat"=>$request['alamat'],
        //         "email"=>$request['email'],
        //         "no_hp"=>$request['no_hp'],
        //         "ceo"=>$request['ceo'],
        //     ]
        // );
        $companies=companies::create([
            "gambar"=>$nama_file,
            "nama_companies"=>$request['nama_companies'],
            "alamat"=>$request['alamat'],
            "email"=>$request['email'],
            "no_hp"=>$request['no_hp'],
            "ceo"=>$request['ceo'],
        ]);
        return redirect('/admin/companies')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function detail($id){
        // $companies=DB::table('companies')->where('id',$id)->first();
        $companies=companies::find($id);
        return view('admin.content.detail.detail_companies',compact('companies'));
    }
    public function delate($id){
        // $query=DB::table('companies')->where('id',$id)->delete();
        companies::destroy($id);
        return redirect('/admin/companies')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $companies=DB::table('companies')->where('id',$id)->first();
        $companies=companies::find($id);
        return view('admin.content.edit.edit_companies',compact('companies'));
    }
    public function update($id,Request $request){
        
        $request->validate([
            'nama_companies'=>'required',
            'alamat'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'ceo'=>'required'
        ]);
        if($request->gambar){
            $file = $request->file('gambar');
        
            // Mendapatkan Nama File
            $nama_file = time()."_".$file->getClientOriginalName();
        
            // Mendapatkan Extension File
            $extension = $file->getClientOriginalExtension();
        
            // Mendapatkan Ukuran File
            $ukuran_file = $file->getSize();
        
            // Proses Upload File
            $destinationPath = 'uploads';
            $file->move($destinationPath,$nama_file);
            // $query=DB::table('companies')->where('id',$id)->update([
            //     "nama_companies"=>$request['nama_companies'],
            //     "alamat"=>$request['alamat'],
            //     "email"=>$request['email'],
            //     "no_hp"=>$request['no_hp'],
            //     "ceo"=>$request['ceo'],
            //     "gambar"=>$nama_file
            // ]);
            $update=companies::where('id',$id)->update([
                "nama_companies"=>$request['nama_companies'],
                "alamat"=>$request['alamat'],
                "email"=>$request['email'],
                "no_hp"=>$request['no_hp'],
                "ceo"=>$request['ceo'],
                "gambar"=>$nama_file
            ]);
        }else{
            // $query=DB::table('companies')->where('id',$id)->update([
            //     "nama_companies"=>$request['nama_companies'],
            //     "alamat"=>$request['alamat'],
            //     "email"=>$request['email'],
            //     "no_hp"=>$request['no_hp'],
            //     "ceo"=>$request['ceo'],
            // ]);
            $update=companies::where('id',$id)->update([
                "nama_companies"=>$request['nama_companies'],
                "alamat"=>$request['alamat'],
                "email"=>$request['email'],
                "no_hp"=>$request['no_hp'],
                "ceo"=>$request['ceo']
            ]);
        }
        
        return redirect('/admin/companies')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
