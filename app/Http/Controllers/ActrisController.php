<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\actris;
use App\kategori_actris;
class ActrisController extends Controller
{
    public function index(){
        // $artis=DB::table('actris')->get();
        // $kategori_artis=DB::table('kategori_actris')->get();
        $artis=actris::all();
        $kategori_artis=kategori_actris::all();
        return view('admin.content.actris',compact('artis','kategori_artis'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $artis=DB::table('actris')->where('nama_panjang','like',"%".$cari."%")->get();
        $artis=actris::where('nama_panjang','like',"%".$cari."%")->get();
        $kategori_artis=kategori_actris::all();
        // mengirim data pegawai ke view index
        return view('admin.content.actris',compact('artis','kategori_artis'));
	}
    public function tambah(){
        $kategori_artis=kategori_actris::all();
        return view('admin.content.add.add_artist',compact('kategori_artis'));
    }
    public function store(Request $request){
        $request->validate([
            'nama_panggilan'=>'required|unique:actris',
            'nama_panjang'=>'required|unique:actris',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'gambar'=>'required',
            'link'=>'required',
            'kategori1'=>'required',
        ]);
        // $query=DB::table('actris')->insert(
        //     [
        //         "nama_panggilan"=>$request['nama_panggilan'],
        //         "nama_panjang"=>$request['nama_panjang'],
        //         "tempat_lahir"=>$request['tempat_lahir'],
        //         "tanggal_lahir"=>$request['tanggal_lahir'],
        //         "email"=>$request['email'],
        //         "no_hp"=>$request['no_hp'],
        //         "alamat"=>$request['alamat'],
        //         "gambar"=>$request['gambar'],
        //         "link"=>$request['link'],
        //         "kategori1"=>$request['kategori1'],
        //         "kategori2"=>$request['kategori2'],
        //         "kategori3"=>$request['kategori3'],
        //     ]
        // );
        $artis=actris::create([
            "nama_panggilan"=>$request['nama_panggilan'],
            "nama_panjang"=>$request['nama_panjang'],
            "tempat_lahir"=>$request['tempat_lahir'],
            "tanggal_lahir"=>$request['tanggal_lahir'],
            "email"=>$request['email'],
            "no_hp"=>$request['no_hp'],
            "alamat"=>$request['alamat'],
            "gambar"=>$request['gambar'],
            "link"=>$request['link'],
            "kategori1"=>$request['kategori1'],
            "kategori2"=>$request['kategori2'],
            "kategori3"=>$request['kategori3'],
        ]);
        return redirect('/admin/actris')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function detail($id){
        // $artis=DB::table('actris')->where('id',$id)->first();
        $kategori_artis=kategori_actris::all();
        $artis=actris::find($id);
        return view('admin.content.detail.detail_artist',compact('artis','kategori_artis'));
    }
    public function delate($id){
        // $query=DB::table('actris')->where('id',$id)->delete();
        actris::destroy($id);
        return redirect('/admin/actris')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $artis=DB::table('actris')->where('id',$id)->first();
        $artis=actris::find($id);
        $kategori_artis=kategori_actris::all();
        return view('admin.content.edit.edit_artist',compact('artis','kategori_artis'));
    }
    public function update($id,Request $request){
        $request->validate([
            'nama_panggilan'=>'required',
            'nama_panjang'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'gambar'=>'required',
            'link'=>'required',
            'kategori1'=>'required',
        ]);
        // $query=DB::table('actris')->where('id',$id)->update([
        //     "nama_panggilan"=>$request['nama_panggilan'],
        //     "nama_panjang"=>$request['nama_panjang'],
        //     "tempat_lahir"=>$request['tempat_lahir'],
        //     "tanggal_lahir"=>$request['tanggal_lahir'],
        //     "email"=>$request['email'],
        //     "no_hp"=>$request['no_hp'],
        //     "alamat"=>$request['alamat'],
        //     "gambar"=>$request['gambar'],
        //     "link"=>$request['link'],
        //     "kategori1"=>$request['kategori1'],
        //     "kategori2"=>$request['kategori2'],
        //     "kategori3"=>$request['kategori3'],
        // ]);
        $update=actris::where('id',$id)->update([
            "nama_panggilan"=>$request['nama_panggilan'],
            "nama_panjang"=>$request['nama_panjang'],
            "tempat_lahir"=>$request['tempat_lahir'],
            "tanggal_lahir"=>$request['tanggal_lahir'],
            "email"=>$request['email'],
            "no_hp"=>$request['no_hp'],
            "alamat"=>$request['alamat'],
            "gambar"=>$request['gambar'],
            "link"=>$request['link'],
            "kategori1"=>$request['kategori1'],
            "kategori2"=>$request['kategori2'],
            "kategori3"=>$request['kategori3'],
        ]);
        return redirect('/admin/actris')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
