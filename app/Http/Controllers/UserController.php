<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
class UserController extends Controller
{
    public function index(){
        $user=User::all();
        return view('admin.content.user',compact('user'));
    }
    public function cari(Request $request){
		$cari = $request->cari;
        $user=User::where('name','like',"%".$cari."%")->get();
        return view('admin.content.user',compact('user'));
	}
    public function tambah(){
        return view('admin.content.add.add_user');
    }
    public function store(Request $request){
        $request->validate([
            'name'=>'required|unique:user',
            'email'=>'required',
            'password'=>'required'
        ]);
        $user=User::create(
            [
                "name"=>$request['name'],
                "email"=>$request['email'],
                "password"=>$request['password']
            ]
        );
        return redirect('/admin/user')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function detail($id){
        $user=User::find($id);
        return view('admin.content.detail.detail_user',compact('user'));
    }
    public function delate($id){
        User::destroy($id);
        return redirect('/admin/user')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        $user=User::find($id);
        return view('admin.content.edit.edit_user',compact('user'));
    }
    public function update($id,Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);
        $update=User::where('id',$id)->update([
            "name"=>$request['name'],
            "email"=>$request['email'],
            "password"=>$request['password']
        ]);
        return redirect('/admin/user')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
