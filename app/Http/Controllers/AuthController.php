<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class AuthController extends Controller
{
    public function getLogin(){
        return view('login.login');
    }

    public function postLogin(Request $request){
        if(!\Auth::attempt(['email' => $request->email,'password' => $request->password])){
            return redirect()->back(); 
        }

        return redirect()->route('halaman_admin');
    }

    public function getRegister(){
        return view('login.register');
    }

    public function postRegister(Request $request){
        $this->validate($request, [
            'name'=> ' required',
            'email' => 'required|email|unique:users',
            'password'=> 'required'
        ]);
       User::create([
           'name' => $request->name,
           'email' => $request->email,
           'password' => bcrypt($request->password)
       ]);

       return redirect()->route('login');
    }

    public function logout(){
        \Auth::logout();

        return redirect()->route('login');
    }
}
