<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\actris;
use App\album;
use App\variety;
use App\movie;
class HalamanAdminController extends Controller
{
    public function count_all(){
        $playload["actris"]=actris::all()->count();
        $playload["album"]=album::all()->count();
        $playload["variety"]=variety::all()->count();
        $playload["movie"]=movie::all()->count();
        return view('admin.content.dashboard',$playload);
    }
}
