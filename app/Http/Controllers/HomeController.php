<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\actris;
use App\kategori_actris;
use App\album;
use App\variety;
use App\movie;
use App\business;
use App\companies;
use App\team;
class HomeController extends Controller
{
    public function index(){
        $artis=actris::all();
        $kategori_artis=kategori_actris::all();
        return view('user.content.artis',compact('artis','kategori_artis'));
    }
    public function index2(){
        $album=album::all();
        return view('user.content.album',compact('album'));
    }
    public function index3(){
        $variety=variety::all();
        return view('user.content.variety',compact('variety'));
    }
    public function index4(){
        $movie=movie::all();
        return view('user.content.movie',compact('movie'));
    }
    public function index5(){
        $companies=companies::all();
        $business1=business::where('kategori_business','1')->get();
        $business2=business::where('kategori_business','2')->get();
        $business3=business::where('kategori_business','3')->get();
        $team=DB::table('team')->get();
        return view('user.content.home',compact('companies','business1','business2','business3','team'));
    }
}
