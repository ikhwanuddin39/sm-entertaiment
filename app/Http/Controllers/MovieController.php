<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\movie;
class MovieController extends Controller
{
    public function index(){
        // $movie=DB::table('movie')->get();
        $movie=movie::all();
        return view('admin.content.movie',compact('movie'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $movie=DB::table('movie')->where('program','like',"%".$cari."%")->get();
        $movie=movie::where('program','like',"%".$cari."%")->get();
        // mengirim data pegawai ke view index
        return view('admin.content.movie',compact('movie'));
	}
    public function tambah(){
        return view('admin.content.add.add_movie');
    }
    public function store(Request $request){
        $request->validate([
            'program'=>'required|unique:movie',
            'penayangan'=>'required',
            'gambar'=>'required',
        ]);
        // $query=DB::table('movie')->insert(
        //     [
        //         "program"=>$request['program'],
        //         "penayangan"=>$request['penayangan'],
        //         "gambar"=>$request['gambar'],
        //     ]
        // );
        $movie=movie::create([
            "program"=>$request['program'],
            "penayangan"=>$request['penayangan'],
            "gambar"=>$request['gambar'],
        ]);
        return redirect('/admin/movie')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function delate($id){
        // $query=DB::table('movie')->where('id',$id)->delete();
        movie::destroy($id);
        return redirect('/admin/movie')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $movie=DB::table('movie')->where('id',$id)->first();
        $movie=movie::find($id);
        return view('admin.content.edit.edit_movie',compact('movie'));
    }
    public function update($id,Request $request){
        $request->validate([
            'program'=>'required',
            'penayangan'=>'required',
            'gambar'=>'required',
        ]);
        // $query=DB::table('movie')->where('id',$id)->update([
        //     "program"=>$request['program'],
        //     "penayangan"=>$request['penayangan'],
        //     "gambar"=>$request['gambar'],
        // ]);
        $update=movie::where('id',$id)->update([
            "program"=>$request['program'],
            "penayangan"=>$request['penayangan'],
            "gambar"=>$request['gambar'],
        ]);
        return redirect('/admin/movie')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
