<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\business;
use App\kategori_business;
class BusinessController extends Controller
{
    public function index(){
        // $business=DB::table('business')->get();
        $kategori_business=kategori_business::all();
        $business=business::all();
        return view('admin.content.business',compact('business','kategori_business'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $business=DB::table('business')->where('nama_business','like',"%".$cari."%")->get();
        $business=business::where('nama_business','like',"%".$cari."%")->get();
        // mengirim data pegawai ke view index
        return view('admin.content.business',compact('business'));
	}
    public function tambah(){
        // $kategori_business=DB::table('kategori_business')->get();
        $kategori_business=kategori_business::all();
        return view('admin.content.add.add_business',compact('kategori_business'));
    }
    public function store(Request $request){
        $file = $request->file('gambar');
        
        // Mendapatkan Nama File
        $nama_file = time()."_".$file->getClientOriginalName();
    
        // Mendapatkan Extension File
        $extension = $file->getClientOriginalExtension();
    
        // Mendapatkan Ukuran File
        $ukuran_file = $file->getSize();
    
        // Proses Upload File
        $destinationPath = 'uploads';
        $file->move($destinationPath,$nama_file);
        $request->validate([
            'nama_business'=>'required|unique:business',
            'jenis_business'=>'required',
            'kategori_business'=>'required',
        ]);
        // $query=DB::table('business')->insert(
        //     [
        //         "gambar"=>$nama_file,
        //         "nama_business"=>$request['nama_business'],
        //         "jenis_business"=>$request['jenis_business'],
        //         "kategori_business"=>$request['kategori_business']
        //     ]
        // );
        $business=business::create([
            "gambar"=>$nama_file,
            "nama_business"=>$request['nama_business'],
            "jenis_business"=>$request['jenis_business'],
            "kategori_business"=>$request['kategori_business']
        ]);
        return redirect('/admin/business')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function delate($id){
        // $query=DB::table('business')->where('id',$id)->delete();
        business::destroy($id);
        return redirect('/admin/business')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $business=DB::table('business')->where('id',$id)->first();
        // $kategori_business=DB::table('kategori_business')->get();
        $business=business::find($id);
        $kategori_business=kategori_business::all();
        return view('admin.content.edit.edit_business',compact('business','kategori_business'));
    }
    public function update($id,Request $request){
        
        $request->validate([
            'nama_business'=>'required',
            'jenis_business'=>'required',
            'kategori_business'=>'required',
        ]);
        if($request->gambar){
            $file = $request->file('gambar');
        
            // Mendapatkan Nama File
            $nama_file = time()."_".$file->getClientOriginalName();
        
            // Mendapatkan Extension File
            $extension = $file->getClientOriginalExtension();
        
            // Mendapatkan Ukuran File
            $ukuran_file = $file->getSize();
        
            // Proses Upload File
            $destinationPath = 'uploads';
            $file->move($destinationPath,$nama_file);
            // $query=DB::table('business')->where('id',$id)->update([
            //     "gambar"=>$nama_file,
            //     "nama_business"=>$request['nama_business'],
            //     "jenis_business"=>$request['jenis_business'],
            //     "kategori_business"=>$request['kategori_business']
            // ]);
            $update=business::where('id',$id)->update([
                "gambar"=>$nama_file,
                "nama_business"=>$request['nama_business'],
                "jenis_business"=>$request['jenis_business'],
                "kategori_business"=>$request['kategori_business']
            ]);
        }else{
            // $query=DB::table('business')->where('id',$id)->update([
            //     "nama_business"=>$request['nama_business'],
            //     "jenis_business"=>$request['jenis_business'],
            //     "kategori_business"=>$request['kategori_business']
            // ]);
            $update=business::where('id',$id)->update([
                "nama_business"=>$request['nama_business'],
                "jenis_business"=>$request['jenis_business'],
                "kategori_business"=>$request['kategori_business']
            ]);
        }
        
        return redirect('/admin/business')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
